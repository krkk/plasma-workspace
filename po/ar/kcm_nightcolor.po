# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2021, 2022, 2024 Zayed Al-Saidi <zayed.alsaidi@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-07 00:38+0000\n"
"PO-Revision-Date: 2024-02-10 17:26+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 23.08.1\n"

#: ui/DayNightView.qml:110
#, kde-format
msgid ""
"Color temperature begins changing to night time at %1 and is fully changed "
"by %2"
msgstr "تبدأ حرارة اللون بالتغير في وقت الليل عند %1 وتكتمل عند %2"

#: ui/DayNightView.qml:113
#, kde-format
msgid ""
"Color temperature begins changing to day time at %1 and is fully changed by "
"%2"
msgstr "تبدأ حرارة اللون بالتغير في وقت النهار عند %1 وتكتمل عند %2"

#: ui/LocationsFixedView.qml:39
#, kde-format
msgctxt ""
"@label:chooser Tap should be translated to mean touching using a touchscreen"
msgid "Tap to choose your location on the map."
msgstr "اطرق لاختيار موقعك على الخريطة."

#: ui/LocationsFixedView.qml:40
#, kde-format
msgctxt ""
"@label:chooser Click should be translated to mean clicking using a mouse"
msgid "Click to choose your location on the map."
msgstr "انقر لاختيار موقعك على الخريطة."

#: ui/LocationsFixedView.qml:79 ui/LocationsFixedView.qml:104
#, kde-format
msgid "Zoom in"
msgstr "قرّب"

#: ui/LocationsFixedView.qml:210
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Modified from <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>World location map</link> by "
"TUBS / Wikimedia Commons / <link url='https://creativecommons.org/licenses/"
"by-sa/3.0'>CC BY-SA 3.0</link>"
msgstr ""
"معدل من  <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>World location map</link> "
"بواسطة TUBS / Wikimedia Commons / <link url='https://creativecommons.org/"
"licenses/by-sa/3.0'>CC BY-SA 3.0</link>"

#: ui/LocationsFixedView.qml:223
#, kde-format
msgctxt "@label: textbox"
msgid "Latitude:"
msgstr "خط العرض:"

#: ui/LocationsFixedView.qml:250
#, kde-format
msgctxt "@label: textbox"
msgid "Longitude:"
msgstr "خط الطول:"

#: ui/main.qml:98
#, kde-format
msgid "The blue light filter makes the colors on the screen warmer."
msgstr "مرشح الضوء الأزرق يجعل الألوان على الشاشة أكثر دفئًا."

#: ui/main.qml:154
#, kde-format
msgid "Switching times:"
msgstr "وقت التبديل:"

#: ui/main.qml:157
#, kde-format
msgid "Always off"
msgstr "دائما معطل"

#: ui/main.qml:158
#, kde-format
msgid "Sunset and sunrise at current location"
msgstr "من غروب الشمس وشروقها في الموقع الحالي"

#: ui/main.qml:159
#, kde-format
msgid "Sunset and sunrise at manual location"
msgstr "غروب الشمس وشروقها في موقع يدوي"

#: ui/main.qml:160
#, kde-format
msgid "Custom times"
msgstr "وقت مخصص"

#: ui/main.qml:161
#, kde-format
msgid "Always on night light"
msgstr "دائما على الإضاءة الليلية"

#: ui/main.qml:184
#, kde-format
msgid "Day light temperature:"
msgstr "حرارة الإضاءة النهارية:"

#: ui/main.qml:227 ui/main.qml:289
#, kde-format
msgctxt "Color temperature in Kelvin"
msgid "%1K"
msgstr "%1 كلفن"

#: ui/main.qml:232 ui/main.qml:294
#, kde-format
msgctxt "Night colour blue-ish; no blue light filter activated"
msgid "Cool (no filter)"
msgstr "بارد (بدون مرشح)"

#: ui/main.qml:239 ui/main.qml:301
#, kde-format
msgctxt "Night colour red-ish"
msgid "Warm"
msgstr "دافئ"

#: ui/main.qml:246
#, kde-format
msgid "Night light temperature:"
msgstr "حرارة الإضاءة الليلية:"

#: ui/main.qml:311
#, kde-format
msgctxt "@label The coordinates for the current location"
msgid "Current location:"
msgstr "الموقع الحالي:"

#: ui/main.qml:317
#, kde-format
msgid "Latitude: %1°   Longitude: %2°"
msgstr "العرض: %1°   الطول: %2°"

#: ui/main.qml:339
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The device's location will be periodically updated using GPS (if available), "
"or by sending network information to <link url='https://location.services."
"mozilla.com'>Mozilla Location Service</link>."
msgstr ""
"سيحدث موقع الجهاز بشكل دوري باستخدام جي بي اس (إذا كان متاحًا)، أو عن طريق "
"إرسال معلومات الشبكة إلى  <link url=\"https://location.services.mozilla.com"
"\">خدمة موقع موزيلا</link>."

#: ui/main.qml:357
#, kde-format
msgid "Begin night light at:"
msgstr "تبدأ الإضاءة الليلية عند:"

#: ui/main.qml:370 ui/main.qml:393
#, kde-format
msgid "Input format: HH:MM"
msgstr "تنسيق الإدخال: س س : دد"

#: ui/main.qml:380
#, kde-format
msgid "Begin day light at:"
msgstr "تبدأ الإضاءة النهارية عند:"

#: ui/main.qml:402
#, kde-format
msgid "Transition duration:"
msgstr "مدة الانتقال:"

#: ui/main.qml:411
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 دقيقة"
msgstr[1] "دقيقة واحدة"
msgstr[2] "دقيقتين"
msgstr[3] "%1 دقائق"
msgstr[4] "%1 دقيقة"
msgstr[5] "%1 دقيقة"

#: ui/main.qml:424
#, kde-format
msgid "Input minutes - min. 1, max. 600"
msgstr "الإدخال دقائق أقل شيء 1 و أكثر شيء 600"

#: ui/main.qml:442
#, kde-format
msgid "Error: Transition time overlaps."
msgstr "خطأ: يتداخل الوقت الانتقالي. "

#: ui/main.qml:465
#, kde-format
msgctxt "@info:placeholder"
msgid "Locating…"
msgstr "يحدد الموقع..."

#~ msgctxt "Night colour blue-ish"
#~ msgid "Cool"
#~ msgstr "بارد"

#~ msgid "This is what day color temperature will look like when active."
#~ msgstr "هكذا تبدو حرارة اللون النهاري عندما تفعل."

#~ msgid "This is what night color temperature will look like when active."
#~ msgstr "هكذا تبدو حرارة اللون الليلي عندما تفعل."

#~ msgid "Activate blue light filter"
#~ msgstr "فعل مرشح الضوء الأرزق"

#~ msgid "Turn on at:"
#~ msgstr "شغّل عند:"

#~ msgid "Turn off at:"
#~ msgstr "أوقف عند:"

#~ msgid "Night Color begins changing back at %1 and ends at %2"
#~ msgstr "يبدأ اللون الليلي بالانحسار عند %1 وينتهي عند %2"

#~ msgid "Error: Morning is before evening."
#~ msgstr "خطأ: الصباح قبل المساء"

#~ msgid ""
#~ "The device's location will be detected using GPS (if available), or by "
#~ "sending network information to <a href=\"https://location.services."
#~ "mozilla.com\">Mozilla Location Service</a>."
#~ msgstr ""
#~ "سيكتشف موقع الجهاز باستخدام جي بي اس (إذا كان متاحًا)، أو عن طريق إرسال "
#~ "معلومات الشبكة إلى <a href=\"https://location.services.mozilla.com\">خدمة "
#~ "موقع موزيلا</a>."

#~ msgid "Night Color begins at %1"
#~ msgstr "يبدأ اللون الليلي عند %1"

#~ msgid "Color fully changed at %1"
#~ msgstr "يتغير للون الليلي بالكامل عند %1"

#~ msgid "Normal coloration restored by %1"
#~ msgstr "يستعاد الألوان العادية عند %1"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "زايد السعيدي"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "zayed.alsaidi@gmail.com"

#~ msgid "Night Color"
#~ msgstr "لون ليلي"

#~ msgid "Roman Gilg"
#~ msgstr "Roman Gilg"
