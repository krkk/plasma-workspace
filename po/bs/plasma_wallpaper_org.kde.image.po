# translation of plasma_wallpaper_image.po to bosanski
# Chusslove Illich <caslav.ilic@gmx.net>, 2009, 2010.
# Dalibor Djuric <dalibor.djuric@mozilla-srbija.org>, 2009, 2010.
# KDE 4 <megaribi@epn.ba>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: plasma_wallpaper_image\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-19 01:35+0000\n"
"PO-Revision-Date: 2015-02-04 14:28+0000\n"
"Last-Translator: Dino Babahmetovic <dbabahmeto1@etf.unsa.ba>\n"
"Language-Team: bosanski <bs@li.org>\n"
"Language: bs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2015-02-05 07:28+0000\n"
"X-Generator: Launchpad (build 17331)\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#: imagepackage/contents/ui/AddFileDialog.qml:57
#, fuzzy, kde-format
#| msgid "Open Image"
msgctxt "@title:window"
msgid "Open Image"
msgstr "Otvori sliku"

#: imagepackage/contents/ui/AddFileDialog.qml:69
#, fuzzy, kde-format
#| msgid "Directory with the wallpaper to show slides from"
msgctxt "@title:window"
msgid "Directory with the wallpaper to show slides from"
msgstr "Direktorij koji sadrži slajdove iz kojih će se prikazati pozadina"

#: imagepackage/contents/ui/config.qml:126
#, kde-format
msgid "Positioning:"
msgstr "Postavljanje:"

#: imagepackage/contents/ui/config.qml:129
#, fuzzy, kde-format
msgid "Scaled and Cropped"
msgstr "Skalirano && Izrezano"

# >> @item:inlistbox Positioning
#: imagepackage/contents/ui/config.qml:133
#, kde-format
msgid "Scaled"
msgstr "Skalirano"

#: imagepackage/contents/ui/config.qml:137
#, kde-format
msgid "Scaled, Keep Proportions"
msgstr "Skalirano, Sačuvane proprcije"

# >> @item:inlistbox Positioning
#: imagepackage/contents/ui/config.qml:141
#, kde-format
msgid "Centered"
msgstr "Centrirano"

# >> @item:inlistbox Positioning
#: imagepackage/contents/ui/config.qml:145
#, kde-format
msgid "Tiled"
msgstr "Popločano"

#: imagepackage/contents/ui/config.qml:173
#, fuzzy, kde-format
#| msgid "Background Color:"
msgid "Background:"
msgstr "Pozadinska boja:"

#: imagepackage/contents/ui/config.qml:174
#, kde-format
msgid "Blur"
msgstr ""

#: imagepackage/contents/ui/config.qml:183
#, kde-format
msgid "Solid color"
msgstr ""

#: imagepackage/contents/ui/config.qml:194
#, fuzzy, kde-format
msgid "Select Background Color"
msgstr "Pozadinska boja:"

#: imagepackage/contents/ui/main.qml:34
#, fuzzy, kde-format
msgid "Open Wallpaper Image"
msgstr "Sljedeća tapetna slika"

#: imagepackage/contents/ui/main.qml:40
#, kde-format
msgid "Next Wallpaper Image"
msgstr "Sljedeća tapetna slika"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:70
#, fuzzy, kde-format
#| msgid "Open Image"
msgid "Images"
msgstr "Otvori sliku"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:74
#, kde-format
msgctxt "@action:button the thing being added is an image file"
msgid "Add…"
msgstr ""

#: imagepackage/contents/ui/ThumbnailsComponent.qml:80
#, fuzzy, kde-format
#| msgid "Download Wallpapers"
msgctxt "@action:button the new things being gotten are wallpapers"
msgid "Get New…"
msgstr "Preuzimanje Pozadina"

#: imagepackage/contents/ui/WallpaperDelegate.qml:31
#, kde-format
msgid "Open Containing Folder"
msgstr ""

#: imagepackage/contents/ui/WallpaperDelegate.qml:37
#, fuzzy, kde-format
#| msgid "Remove wallpaper"
msgid "Restore wallpaper"
msgstr "Ukloni pozadinu"

#: imagepackage/contents/ui/WallpaperDelegate.qml:42
#, fuzzy, kde-format
#| msgid "Remove wallpaper"
msgid "Remove Wallpaper"
msgstr "Ukloni pozadinu"

#: plasma-apply-wallpaperimage.cpp:29
#, kde-format
msgid ""
"This tool allows you to set an image as the wallpaper for the Plasma session."
msgstr ""

#: plasma-apply-wallpaperimage.cpp:31
#, kde-format
msgid ""
"An image file or an installed wallpaper kpackage that you wish to set as the "
"wallpaper for your Plasma session"
msgstr ""

#: plasma-apply-wallpaperimage.cpp:45
#, kde-format
msgid ""
"There is a stray single quote in the filename of this wallpaper (') - please "
"contact the author of the wallpaper to fix this, or rename the file "
"yourself: %1"
msgstr ""

#: plasma-apply-wallpaperimage.cpp:85
#, kde-format
msgid "An error occurred while attempting to set the Plasma wallpaper:\n"
msgstr ""

#: plasma-apply-wallpaperimage.cpp:89
#, kde-format
msgid ""
"Successfully set the wallpaper for all desktops to the KPackage based %1"
msgstr ""

#: plasma-apply-wallpaperimage.cpp:91
#, kde-format
msgid "Successfully set the wallpaper for all desktops to the image %1"
msgstr ""

#: plasma-apply-wallpaperimage.cpp:97
#, kde-format
msgid ""
"The file passed to be set as wallpaper does not exist, or we cannot identify "
"it as a wallpaper: %1"
msgstr ""

#. i18n people, this isn't a "word puzzle". there is a specific string format for QFileDialog::setNameFilters
#: plugin/imagebackend.cpp:336
#, kde-format
msgid "Image Files"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:48
#, kde-format
msgid "Order:"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:55
#, kde-format
msgid "Random"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:59
#, kde-format
msgid "A to Z"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:63
#, kde-format
msgid "Z to A"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:67
#, kde-format
msgid "Date modified (newest first)"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:71
#, kde-format
msgid "Date modified (oldest first)"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:96
#, kde-format
msgid "Group by folders"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:108
#, kde-format
msgid "Change every:"
msgstr "Promjeni sve:"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:118
#, kde-format
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:138
#, fuzzy, kde-format
#| msgid "Minutes"
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "Minute"
msgstr[1] "Minute"
msgstr[2] "Minute"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:158
#, fuzzy, kde-format
#| msgid "Seconds"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "Sekunde"
msgstr[1] "Sekunde"
msgstr[2] "Sekunde"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:189
#, fuzzy, kde-format
#| msgid "Remove wallpaper"
msgid "Folders"
msgstr "Ukloni pozadinu"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:193
#, kde-format
msgctxt "@action button the thing being added is a folder"
msgid "Add…"
msgstr ""

#: slideshowpackage/contents/ui/SlideshowComponent.qml:231
#, fuzzy, kde-format
#| msgid "Remove wallpaper"
msgid "Remove Folder"
msgstr "Ukloni pozadinu"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:242
#, fuzzy, kde-format
#| msgid "Add Folder"
msgid "Open Folder…"
msgstr "Dodaj direktorij"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:257
#, kde-format
msgid "There are no wallpaper locations configured"
msgstr ""

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:49
#, fuzzy, kde-format
#| msgid "Download Wallpapers"
msgctxt "@action:inmenu"
msgid "Set as Wallpaper"
msgstr "Preuzimanje Pozadina"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:52
#, kde-format
msgctxt "@action:inmenu Set as Desktop Wallpaper"
msgid "Desktop"
msgstr ""

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:58
#, kde-format
msgctxt "@action:inmenu Set as Lockscreen Wallpaper"
msgid "Lockscreen"
msgstr ""

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:64
#, kde-format
msgctxt "@action:inmenu Set as both lockscreen and Desktop Wallpaper"
msgid "Both"
msgstr ""

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:97
#, kde-kuit-format
msgctxt "@info %1 is the dbus error message"
msgid "An error occurred while attempting to set the Plasma wallpaper:<nl/>%1"
msgstr ""

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:110
#, kde-format
msgid "An error occurred while attempting to open kscreenlockerrc config file."
msgstr ""

#, fuzzy
#~| msgid "Add Folder"
#~ msgid "Add Folder…"
#~ msgstr "Dodaj direktorij"

#~ msgid "Recommended wallpaper file"
#~ msgstr "Preporučena datoteka pozadinske slike"

#, fuzzy
#~| msgid "Remove wallpaper"
#~ msgid "Add Custom Wallpaper"
#~ msgstr "Ukloni pozadinu"

#~ msgid "Remove wallpaper"
#~ msgstr "Ukloni pozadinu"

#~ msgid "%1 by %2"
#~ msgstr "%1 od %2"

#, fuzzy
#~| msgid "Remove wallpaper"
#~ msgid "Wallpapers"
#~ msgstr "Ukloni pozadinu"

#~ msgid "Download Wallpapers"
#~ msgstr "Preuzimanje Pozadina"

#~ msgid "Hours"
#~ msgstr "Sati"

#~ msgid "Open..."
#~ msgstr "Otvori..."

#~ msgctxt "Unknown Author"
#~ msgid "Unknown"
#~ msgstr "Nepoznato"

#~ msgid "Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz *.xcf)"
#~ msgstr "Datoteke slika (*.png *.jpg *.jpeg *.bmp *.svg *.svgz *.xcf)"
