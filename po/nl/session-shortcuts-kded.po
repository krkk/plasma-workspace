# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2024 Freek de Kruijf <freekdekruijf@kde.nl>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-30 00:39+0000\n"
"PO-Revision-Date: 2024-01-30 13:33+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.4\n"

#: main.cpp:32
#, kde-format
msgid "Session Management"
msgstr "Sessiebeheer"

#: main.cpp:39
#, kde-format
msgctxt "@action"
msgid "Show Logout Prompt"
msgstr "Afmeldprompt tonen"

#: main.cpp:48
#, kde-format
msgid "Log Out"
msgstr "Afmelden"

#: main.cpp:54
#, kde-format
msgid "Shut Down"
msgstr "Systeem afsluiten"

#: main.cpp:60
#, kde-format
msgid "Reboot"
msgstr "Systeem herstarten"

#: main.cpp:68
#, kde-format
msgid "Log Out Without Confirmation"
msgstr "Afmelden zonder bevestiging"

#: main.cpp:74
#, kde-format
msgid "Shut Down Without Confirmation"
msgstr "Afsluiten zonder bevestiging"

#: main.cpp:80
#, kde-format
msgid "Reboot Without Confirmation"
msgstr "Computer herstarten zonder bevestiging"
